package com.db2692.arcitcbot.command;


public interface CommandEntry{

    String getName();
    CommandResponse execute(String[] args, String userName);
    String getPermissions();



}
