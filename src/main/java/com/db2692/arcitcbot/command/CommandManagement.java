package com.db2692.arcitcbot.command;

import com.db2692.arcitcbot.config.TwitchEventManager;
import com.db2692.arcitcbot.config.UserWhitelist;
import com.db2692.arcitcbot.fileio.CommandRepository;

import java.util.Map;

public class CommandManagement extends WhitelistedCommand {

    private TwitchEventManager anEvent;
    private final CommandRepository commandRepository;


    public CommandManagement(UserWhitelist userWhitelist, TwitchEventManager someEvent, CommandRepository commandRepository) {
        super(userWhitelist);
        this.anEvent = someEvent;
        this.commandRepository = commandRepository;
    }

    @Override
    public String getName() {
        return "!command";
    }

    @Override
    public CommandResponse executeAuthorized(String[] args, String userName) {
        String output = args[1] + " is not a valid command.";
            if ("Add".equalsIgnoreCase(args[1])) {
                StringBuilder response = new StringBuilder();

                for (int i = 3; i < args.length; i++) {
                    response.append(args[i]).append(" ");
                }

                SimpleCommand newCommand = new SimpleCommand(args[2], response.toString().trim());

                commandRepository.add(newCommand);
                output = "Command added.";
            }

            if ("Edit".equalsIgnoreCase(args[1])) {
                Map<String, CommandEntry> commandMap = anEvent.getCommands(anEvent);
                CommandEntry commandEntry = commandMap.get(args[2]);
                if("Perm".equalsIgnoreCase(args[3])) {

                    if (commandEntry instanceof SimpleCommand) {
                        if(args[4].equalsIgnoreCase("EVERYONE") || args[4].equalsIgnoreCase("SUBSCRIBER") || args[4].equalsIgnoreCase("MODERATOR") || args[4].equalsIgnoreCase("BROADCASTER")) {
                            ((SimpleCommand) commandEntry).setPermissions(args[4]);

                        }
                        commandRepository.edit((SimpleCommand) commandEntry);
                    }

                    output = "Permissions updated.";
                }
                else{
                    StringBuilder response = new StringBuilder();

                    for (int i = 3; i < args.length; i++) {
                        response.append(args[i]).append(" ");
                    }

                    if (commandEntry instanceof SimpleCommand) {
                        ((SimpleCommand) commandEntry).setResponse(response.toString().trim());
                        commandRepository.edit((SimpleCommand) commandEntry);
                    }

                    output = "Command edited.";
                }

            }

            if ("Delete".equalsIgnoreCase(args[1])){
                commandRepository.remove(args[2]);

                output = "Command removed.";
            }

        return new CommandResponse(output, true);
    }




}
