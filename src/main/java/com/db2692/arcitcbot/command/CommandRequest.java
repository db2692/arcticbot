package com.db2692.arcitcbot.command;

public class CommandRequest {
    private String name;
    private String response;

    public CommandRequest(){}

    public CommandRequest(String name, String response) {

        this.name = name;
        this.response = response;
    }

    public String getName(){
        return name;
    }

    public String getResponse(){
        return response;
    }

    public void setName(String newName){ name = newName; }
}
