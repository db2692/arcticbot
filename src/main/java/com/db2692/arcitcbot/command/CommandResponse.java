package com.db2692.arcitcbot.command;

import lombok.Data;

@Data
public class CommandResponse {
    private String response;
    private boolean privateCommand;

    public CommandResponse(String response, boolean b) {
        this.response = response;
        this.privateCommand = b;
    }


}
