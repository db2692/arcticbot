package com.db2692.arcitcbot.command;

import com.db2692.arcitcbot.twitch.StreamInfoAPI;

public class GetGame implements CommandEntry {
    private StreamInfoAPI twitchAPI;

    public GetGame(StreamInfoAPI twitchAPI) {
        this.twitchAPI = twitchAPI;

    }


    @Override
    public CommandResponse execute(String[] args, String userName) {

        return new CommandResponse(twitchAPI.getGame().toString(), false);
    }

    @Override
    public String getName() {
        return "!game";
    }

    public String getPermissions(){return "EVERYONE";}
}
