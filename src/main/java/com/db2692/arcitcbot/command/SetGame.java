package com.db2692.arcitcbot.command;

import com.db2692.arcitcbot.config.UserWhitelist;
import com.db2692.arcitcbot.twitch.StreamInfoAPI;

public class SetGame extends WhitelistedCommand {
    private StreamInfoAPI twitchAPI;

    public SetGame(UserWhitelist userWhitelist, StreamInfoAPI twitchAPI) {
        super(userWhitelist);
        this.twitchAPI = twitchAPI;

    }

    @Override
    protected CommandResponse executeAuthorized(String[] args, String userName) {
        StringBuilder game = new StringBuilder();

        for (int i = 1; i < args.length; i++) {
            game.append(args[i]).append(" ");
        }
        System.out.println(twitchAPI.searchGames(game.toString().trim()));
        twitchAPI.updateGame(game.toString().trim());
        return new CommandResponse("Game has been updated.", true);
    }

    @Override
    public String getName() {
        return "!setgame";
    }
}

// use https://dev.twitch.tv/docs/v5/reference/search/#search-games