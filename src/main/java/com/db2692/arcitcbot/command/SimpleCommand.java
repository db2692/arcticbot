package com.db2692.arcitcbot.command;

import java.util.Objects;

public class SimpleCommand implements CommandEntry {

    private String name;
    private String response;
    private String permissions;

    public SimpleCommand(){

    }

    public SimpleCommand(String name, String response) {
        this.name = name;
        this.response = response;
        permissions = "EVERYONE";
    }


    @Override
    public CommandResponse execute(String[] args, String userName) {
        return new CommandResponse(response, false);
    }

    public String getResponse(){
        return response;
    }

    public void setResponse(String newResponse){
        this.response = newResponse;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleCommand that = (SimpleCommand) o;
        return name.equals(that.name) &&
                response.equals(that.response);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, response);
    }
}
