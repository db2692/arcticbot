package com.db2692.arcitcbot.command;


import com.db2692.arcitcbot.config.UserWhitelist;

public abstract class WhitelistedCommand implements CommandEntry {

    private UserWhitelist userWhitelist;

    public WhitelistedCommand(UserWhitelist userWhitelist){

        this.userWhitelist = userWhitelist;
    }

    @Override
    public CommandResponse execute(String[] args, String userName) {
        if (userWhitelist.isAuthorized(userName)) {
            return executeAuthorized(args, userName);
        }
        return executeUnauthorized(args, userName);
    }

    private CommandResponse executeUnauthorized(String[] args, String userName) {
        return new CommandResponse("User is not authorized.", true);
    }

    protected abstract CommandResponse executeAuthorized(String[] args, String userName);

    public String getPermissions(){return null;}

}
