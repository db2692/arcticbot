package com.db2692.arcitcbot.twitch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwitchUser {
    private String display_name;
    private String name;
    private long _id;
}
