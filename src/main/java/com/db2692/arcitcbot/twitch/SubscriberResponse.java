package com.db2692.arcitcbot.twitch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriberResponse {
    private long _total;
    private List<TwitchSubscriber> subscriptions;

}
