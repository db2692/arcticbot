package com.db2692.arcitcbot.twitch;

import com.github.philippheuer.credentialmanager.domain.OAuth2Credential;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriUtils;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class StreamInfoAPI {
    RestTemplate restTemplate= new RestTemplate();
    private OAuth2Credential accessToken;

    public StreamInfoAPI(OAuth2Credential accessToken){
        this.accessToken = accessToken;
    }


    public HttpHeaders makeHeaders(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Accept", "application/vnd.twitchtv.v5+json");
        httpHeaders.add("Authorization", "OAuth " + accessToken.getAccessToken());

        return httpHeaders;
    }


    public String getGame()
    {
        RequestEntity<String> requestEntity = new RequestEntity<>(makeHeaders(), HttpMethod.GET, URI.create("https://api.twitch.tv/kraken/channel"));
        ResponseEntity<ChannelResponse> someInfo = restTemplate.exchange(requestEntity, ChannelResponse.class);
        System.out.println(someInfo.getBody());

        return someInfo.getBody().getGame();
    }


    public SubscriberResponse getSubscriberPage(long offset){
        RequestEntity<String> requestEntity = new RequestEntity<>(makeHeaders(), HttpMethod.GET, URI.create("https://api.twitch.tv/kraken/channels/32061771/subscriptions?limit=100&offset=" + offset));
        ResponseEntity<SubscriberResponse> subInformation = restTemplate.exchange(requestEntity, SubscriberResponse.class);

        return subInformation.getBody();
    }


    public List<String> getSubscribers()
    {
        List<String> subscriberList = new ArrayList<>();
        SubscriberResponse subResponse = null;
        long totalSubs = getSubscriberPage(0).get_total();


        for (long offset = 0; offset < totalSubs; offset += 100){
            subResponse = getSubscriberPage(offset);
            List<TwitchSubscriber> parseList = subResponse.getSubscriptions();

            subscriberList.addAll(parseList.stream().map(u -> u.getUser().getName()).collect(Collectors.toList()));



        }


        return subscriberList;
    }

    public void updateGame(String game){
        Channel channelGame = new Channel();
        channelGame.setGame(game);

        ChannelUpdateRequest updateGameRequest = new ChannelUpdateRequest();
        updateGameRequest.setChannel(channelGame);

        RequestEntity<ChannelUpdateRequest> requestEntity = new RequestEntity<>(updateGameRequest, makeHeaders(), HttpMethod.PUT, URI.create("https://api.twitch.tv/kraken/channels/32061771"));
        ResponseEntity<ChannelResponse> someInfo = restTemplate.exchange(requestEntity, ChannelResponse.class);
    }


    public String getTeams(){
        List<String> teamsList = new ArrayList<>();

        RequestEntity<String> requestEntity = new RequestEntity<>(makeHeaders(), HttpMethod.GET, URI.create("https://api.twitch.tv/kraken/channels/32061771/teams"));
        ResponseEntity<TeamResponse> teamInfo = restTemplate.exchange(requestEntity, TeamResponse.class);
        System.out.println(teamInfo.getBody());

        return teamInfo.getBody().toString();
    }

    public List<String> searchGames(String game){
        String encodedGame = UriUtils.encodeQueryParam(game, "UTF-8");

        List<String> gameSuggestions = new ArrayList<>();
        String getGameURL = "https://api.twitch.tv/kraken/search/games?query=" + encodedGame;

        RequestEntity<String> requestEntity = new RequestEntity<>(makeHeaders(), HttpMethod.GET, URI.create(getGameURL));
        ResponseEntity<SearchResponse> gameInfo = restTemplate.exchange(requestEntity, SearchResponse.class);

        System.out.println(gameInfo.getBody().toString());

        return gameSuggestions;
    }
}
