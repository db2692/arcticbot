package com.db2692.arcitcbot.config;

import com.db2692.arcitcbot.command.*;
import com.db2692.arcitcbot.fileio.CommandRepository;
import com.db2692.arcitcbot.fileio.CommandUpdateSubscriber;
import com.db2692.arcitcbot.twitch.StreamInfoAPI;
import com.github.philippheuer.events4j.EventManager;
import com.github.twitch4j.chat.events.channel.ChannelMessageEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class TwitchEventManager extends EventManager implements CommandUpdateSubscriber {
    Map<String, CommandEntry> commandMap = new HashMap<>();
    ChannelMessageEvent newEvent;


    public TwitchEventManager(UserWhitelist authorizedUsers, StreamInfoAPI twitchAPI, CommandRepository fileIO){
        addCommand(new SimpleCommand("!sub", "https://www.twitch.tv/subs/vaecon"));
        addCommand(new CommandManagement(authorizedUsers, this, fileIO));
        addCommand(new SetGame(authorizedUsers, twitchAPI));
        addCommand(new GetGame(twitchAPI));
        addCommand(new GetCommands());

        fileIO.load().forEach(this::addCommand);
        fileIO.subscribe(this);
    }

    public Map<String, CommandEntry> getCommands(TwitchEventManager someEvent){
        return someEvent.commandMap;
    }

    public void addCommand(CommandEntry someCommand){
        commandMap.put(someCommand.getName(), someCommand);

    }

    public void onChat(ChannelMessageEvent cmEvent) {
        System.out.println(cmEvent.getUser() + " " + cmEvent.getMessage());
        if (cmEvent.getMessage().equalsIgnoreCase("Hi")) {
            cmEvent.getTwitchChat().sendMessage(cmEvent.getChannel().getName(), "Hi, " + cmEvent.getUser().getName());

        }
        if (cmEvent.getMessage().startsWith("!")){

            String[] args = cmEvent.getMessage().split(" ");
            if (commandMap.containsKey(args[0])){
                CommandEntry commandEntry = commandMap.get(args[0]);
                if (cmEvent.getPermissions().stream().anyMatch(commandPermission -> commandEntry.getPermissions()==null || commandEntry.getPermissions().equalsIgnoreCase(commandPermission.name()))){
                    CommandResponse commandResponse = commandEntry.execute(args, cmEvent.getUser().getName());
                    if(commandResponse.isPrivateCommand()) {
                        cmEvent.getTwitchChat().sendPrivateMessage(cmEvent.getUser().getName(), commandResponse.getResponse());
                    } else {
                        cmEvent.getTwitchChat().sendMessage(cmEvent.getChannel().getName(), commandResponse.getResponse());
                    }
                }

                System.out.println(cmEvent.getPermissions().stream().map(x -> x.name()).collect(Collectors.joining(",")));

            }



        }
    }

    @Override
    public void onSave(SimpleCommand aCommand) {
        commandMap.put(aCommand.getName(), aCommand);
    }

    @Override
    public void onDelete(SimpleCommand aCommand) {
        commandMap.remove(aCommand.getName());
    }

}
