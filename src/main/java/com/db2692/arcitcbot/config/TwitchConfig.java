package com.db2692.arcitcbot.config;

import com.db2692.arcitcbot.fileio.CommandRepository;
import com.db2692.arcitcbot.twitch.StreamInfoAPI;
import com.github.philippheuer.credentialmanager.CredentialManager;
import com.github.philippheuer.credentialmanager.CredentialManagerBuilder;
import com.github.philippheuer.credentialmanager.domain.OAuth2Credential;
import com.github.philippheuer.events4j.EventManager;
import com.github.twitch4j.TwitchClient;
import com.github.twitch4j.TwitchClientBuilder;
import com.github.twitch4j.auth.providers.TwitchIdentityProvider;
import com.github.twitch4j.chat.events.channel.ChannelMessageEvent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TwitchConfig {

    @Value("${TWITCH_CLIENT_KEY}")
    private String clientID;
    @Value("${TWITCH_CLIENT_SECRET}")
    private String clientSecret;
    @Value("${TWITCH_USER_AUTH}")
    private String userAuth;
    @Value("${TWITCH_BOT_AUTH}")
    private String botAuth;

    @Bean
    public OAuth2Credential oAuth2Credential(){
        return new OAuth2Credential("twitch", userAuth);

    }

    public OAuth2Credential oAuth2ChatCredential(){
        return new OAuth2Credential("twitch", botAuth);
    }

    @Bean
    public TwitchClient twitchClient(CredentialManager credentialManager, OAuth2Credential oAuth2Credential, EventManager eventManager, OAuth2Credential oAuth2ChatCredential){
        return TwitchClientBuilder.builder()
                .withClientId(clientID)
                .withClientSecret(clientSecret)
                .withEnableHelix(true)
                .withEnableChat(true)
                .withEventManager(eventManager)
                .withChatAccount(oAuth2ChatCredential)
                .withCredentialManager(credentialManager)
                .build();
    }

    @Bean
    public CredentialManager credentialManager(){
        CredentialManager credentialManager = CredentialManagerBuilder.builder().build();
        credentialManager.registerIdentityProvider(new TwitchIdentityProvider(clientID, clientSecret, ""));

        return credentialManager;
    }

    @Bean
    public EventManager eventManager(UserWhitelist authorizedUsers, StreamInfoAPI twitchAPI, CommandRepository commandRepository){
        TwitchEventManager eventManager = new TwitchEventManager(authorizedUsers, twitchAPI, commandRepository);

        eventManager.onEvent(ChannelMessageEvent.class).subscribe(eventManager::onChat);

        return eventManager;
    }

    @Bean
    public StreamInfoAPI twitchAPI(OAuth2Credential oAuth2Credential){
        StreamInfoAPI twitchAPI = new StreamInfoAPI(oAuth2Credential);
        return twitchAPI;
    }

    @Bean
    public UserWhitelist userAuthentication(StreamInfoAPI twitchAPI){
        UserWhitelist authorizedUsers = new UserWhitelist();
        return authorizedUsers;
    }



}
