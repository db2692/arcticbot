package com.db2692.arcitcbot.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
public class DatabaseConfig {

    @Bean
    public Connection connection(){
       String connectionString = "jdbc:sqlite:userDB";
        try {
            Connection sqlConnection = DriverManager.getConnection(connectionString);
            return sqlConnection;

        } catch (SQLException e) {
            throw new RuntimeException("Unable to connect to database.", e);
        }

    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource connection){
        return new JdbcTemplate(connection);
    }

    @Bean
    public DataSource dataSource(Connection connection){
        return new SingleConnectionDataSource(connection, true);
    }
}
