package com.db2692.arcitcbot.fileio;

import com.db2692.arcitcbot.command.SimpleCommand;

public interface CommandUpdateSubscriber {

    void onSave(SimpleCommand aCommand);

    void onDelete(SimpleCommand aCommand);
}
