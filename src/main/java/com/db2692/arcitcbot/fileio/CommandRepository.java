package com.db2692.arcitcbot.fileio;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.db2692.arcitcbot.command.SimpleCommand;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@Repository
public class CommandRepository {
    private final String pathname = "C:\\Users\\warfi\\Desktop\\Test\\commands.json";
    List<SimpleCommand> commands = new ArrayList<SimpleCommand>();
    ObjectMapper commandList = new ObjectMapper();
    List<CommandUpdateSubscriber> subList = new ArrayList<>();

    public void subscribe(CommandUpdateSubscriber subscriber){
        subList.add(subscriber);

    }

    public void add(SimpleCommand newCommand){
        for(SimpleCommand aCommand : commands ){
            if(aCommand.getName().equals(newCommand.getName())){
                throw new RuntimeException("Command already exists");
            }
        }
        commands.add(newCommand);

        save();
        subList.forEach(s -> s.onSave(newCommand));
    }

    public void remove(String commandName){
        ListIterator<SimpleCommand> iterator = commands.listIterator();
        SimpleCommand toRemove = null;
        while(iterator.hasNext()){


            SimpleCommand next = iterator.next();
            if(next.getName().equals(commandName)){

                toRemove = next;
                iterator.remove();
            }
        }

        save();
        if(toRemove != null){
            for (CommandUpdateSubscriber s : subList) {
                s.onDelete(toRemove);
            }
        }
    }

    public void edit(SimpleCommand specificCommand){
        for(SimpleCommand aCommand : commands ){
            if(aCommand.getName().equals(specificCommand.getName())) {
                aCommand.setResponse(specificCommand.getResponse());
            }
        }
        save();
        subList.forEach(s -> s.onSave(specificCommand));
    }

    public void save(){
        try{

            commandList.writerWithDefaultPrettyPrinter().writeValue(new File(pathname), commands); // this will overwrite old file
        }
        catch(Exception x){
            System.out.println("Something went wrong");
        }
    }

    public List<SimpleCommand> load(){
        File file = new File(pathname);
        try {
            commands = commandList.readValue(file, new TypeReference<List<SimpleCommand>>(){});
            //System.out.println(commands);
        }
        catch (JsonMappingException e){
            e.printStackTrace();
        }
        catch (JsonGenerationException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        return commands;
    }


}
