package com.db2692.arcitcbot.controller;

public class Greeting {
    private final String name;
    private final String message;

    public Greeting(String name) {
        this.name = name;
        this.message = "Hello " + name;
    }

    public String getMessage() {
        return message;
    }
}
