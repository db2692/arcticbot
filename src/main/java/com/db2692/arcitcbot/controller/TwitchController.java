package com.db2692.arcitcbot.controller;

import com.db2692.arcitcbot.command.CommandRequest;
import com.db2692.arcitcbot.command.SimpleCommand;
import com.db2692.arcitcbot.fileio.CommandRepository;
import com.db2692.arcitcbot.twitch.StreamInfoAPI;
import com.github.philippheuer.credentialmanager.domain.OAuth2Credential;
import com.github.twitch4j.TwitchClient;
import com.github.twitch4j.helix.domain.BitsLeaderboard;
import com.github.twitch4j.helix.domain.Game;
import com.github.twitch4j.helix.domain.User;
import com.github.twitch4j.helix.domain.UserList;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@CrossOrigin("http://localhost:4200")
@RestController
public class TwitchController {

    private TwitchClient twitchClient;
    private OAuth2Credential accessToken;
    private StreamInfoAPI twitchAPI;
    private CommandRepository commandRepository;

    @Autowired
    public TwitchController(TwitchClient twitchClient, OAuth2Credential accessToken, StreamInfoAPI twitchAPI, CommandRepository commandRepository){
        this.twitchClient = twitchClient;
        this.accessToken = accessToken;
        this.twitchAPI = twitchAPI;
        this.commandRepository = commandRepository;
    }

    @GetMapping("twitch")
    public String explodeTwitch(){
        Long gameId = twitchClient.getHelix().getStreams(null, null, null, null, null, null, null, Collections.singletonList("vaecon")).execute().getStreams().get(0).getGameId();
        Game game = twitchClient.getHelix().getGames(Collections.singletonList(gameId + ""), null).execute().getGames().get(0);


        return game.getBoxArtUrl();
    }

    @GetMapping("bits")
    public List<LeaderboardEntry> getBitLeaderboard(){

        BitsLeaderboard resultList = twitchClient.getHelix().getBitsLeaderboard(accessToken.getAccessToken(), "25", "month", "2019-02-01T15:04:05Z", null).execute();
        List<LeaderboardEntry> leaderboard = new ArrayList<>();
        List<@NonNull Long> longs = resultList.getEntries().stream().map(e -> e.getUserId()).collect(Collectors.toList());

        Map<Long, User> userById = getUsers(longs);
        resultList.getEntries().forEach(entry -> {

            LeaderboardEntry anEntry = new LeaderboardEntry(
                    entry.getUserId(),
                    userById.get(entry.getUserId()).getDisplayName(),
                    entry.getRank(),
                    entry.getScore()
            );

            leaderboard.add(anEntry);

        });
        return leaderboard;
    }

    @GetMapping("game")
    public String getGame(){


        return twitchAPI.getGame();
    }

    @GetMapping("setGame")
    public void setGame(){

        twitchAPI.updateGame("Satisfactory");
    }

    @GetMapping("teams")
    public String getTeam(){

        return twitchAPI.getTeams();
    }

    @GetMapping("subs")
    public List<String> getSubs(){


        return twitchAPI.getSubscribers();
    }

    public Map<Long, User> getUsers(List<Long> userID){
        UserList resultList = twitchClient.getHelix().getUsers(null, userID, null).execute();

        return resultList.getUsers().stream().collect(Collectors.toMap((User::getId), Function.identity()));
    }

    @GetMapping
    public String greet(){
        return "Dan is awesome?";
    }

    @ResponseBody
    @GetMapping(value = "/greet/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Greeting smartGreet(@PathVariable("name") String name){
        return new Greeting(name);
    }
}

