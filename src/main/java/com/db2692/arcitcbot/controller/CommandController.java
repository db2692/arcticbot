package com.db2692.arcitcbot.controller;


import com.db2692.arcitcbot.command.CommandRequest;
import com.db2692.arcitcbot.command.SimpleCommand;
import com.db2692.arcitcbot.fileio.CommandRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin("http://localhost:4200")
@RestController
public class CommandController {
    private CommandRepository commandRepository;

    CommandController(CommandRepository commandRepository){
        this.commandRepository = commandRepository;
    }

    @GetMapping("command")
    public List<CommandRequest> getCommands(){
        return commandRepository.load().stream().map(it -> new CommandRequest(it.getName(), it.getResponse())).collect(Collectors.toList());
    }

    @PostMapping("command")
    public CommandRequest addCommand(@RequestBody CommandRequest newCommand){

        if(!newCommand.getName().startsWith("!")){
            newCommand.setName("!" + newCommand.getName());

        }

        commandRepository.add(new SimpleCommand(newCommand.getName(), newCommand.getResponse()));
        return newCommand;
    }

    @DeleteMapping("command/{id}")
    public String removeCommand(@PathVariable("id") String commandName){
        commandRepository.remove(commandName);
        return "Success";
    }

    @PutMapping("command")
    public CommandRequest editCommand(@RequestBody CommandRequest specificCommand){

        commandRepository.edit(new SimpleCommand(specificCommand.getName(), specificCommand.getResponse()));

        return specificCommand;
    }


}
