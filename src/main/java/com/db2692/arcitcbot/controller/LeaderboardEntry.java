package com.db2692.arcitcbot.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LeaderboardEntry{
    private long userID;
    private String username;
    private int rank;
    private int score;

}
